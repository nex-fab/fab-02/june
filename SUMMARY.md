# Summary

## FAB-02
* [1. Project manage](https://www.nexmaker.com/doc/1projectmanage/Assessment1project-manage.html)
  * [Introduce myself](1projectmanage/introducemyself.md)
  * [My final project ](1projectmanage/finalprojectplan.md)

* [2. cad](https://www.nexmaker.com/doc/2cad/Assessment.html)
  * [practive](2cad/practive.md)
* [3. 3d printer](https://www.nexmaker.com/doc/3_3dprinter/assignment.html)
  * [ball](33dprinter/ball.md)
*  [4. arduino](https://www.nexmaker.com/doc/5arduino/assessment.html)
    *  [dmdisplay](4arduino/dmdisplay.md)
*    [5. laser cutting](https://www.nexmaker.com/doc/6laser_cutter/Assessment.html)
    *  [class](5lasercutting/class.md)