1.The solidwork format export into .stl format
2.Open the .stl file with software Cura.
![ab7nOI.jpg](https://gitlab.com/ottopicbed/bed2/uploads/4512bf0bf26fd00acb2498776dd55153/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20200818110430.png)
3.adjust the size and angle
![abvgwd.jpg](https://gitlab.com/ottopicbed/bed2/uploads/250c6310a1989c0c4930cec148eb2b5f/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20200818111230.png)
4.Set the printing parameter
![abxJ9P.jpg](https://gitlab.com/ottopicbed/bed2/uploads/f7ce4a51891aee4c3aecd47a548fe173/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20200818111705.png)

#####Special note:Infill, top and bottom speed low down; Support, check the printing area out of panel; check the original point location.

![](https://gitlab.com/ottopicbed/bed2/uploads/f02e440fc1b58e3f6d834810300f9a66/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20200814102351.png)
5.preview and save to file
6.copy in SD, insert the Card
7.Stat the machine, pick the related files, printing.

finish product
![abxZp6.jpg](https://gitlab.com/ottopicbed/bed2/uploads/b5cb16f822f01e5db1ba0264b65a9186/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20200818121834.jpg)

We used diagonal pliers to remove the support.
![wCS0CF.png](https://s1.ax1x.com/2020/09/03/wCS0CF.png)