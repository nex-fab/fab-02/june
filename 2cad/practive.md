####1.先在草图的中心位置画一个圆
![ab7nOI.jpg](https://s1.ax1x.com/2020/08/10/ab7nOI.jpg)
####2.做半圆，然后实体等距封闭半圆
![abvgwd.jpg](https://s1.ax1x.com/2020/08/10/abvgwd.jpg)
####3.旋转成空心球
![abxJ9P.jpg](https://s1.ax1x.com/2020/08/10/abxJ9P.jpg)
####4.在空心球上新建基准面
![abxZp6.jpg](https://s1.ax1x.com/2020/08/10/abxZp6.jpg)
####5.把多余实体裁剪掉
![abxfHJ.jpg](https://s1.ax1x.com/2020/08/10/abxfHJ.jpg)
####6.在得到开口空空心球后，新建一个组装体
####7.开口实体引用，得到底座， 反向得到内扣.
![abzU8x.jpg](https://s1.ax1x.com/2020/08/10/abzU8x.jpg)
####8.底座处画小圆拉伸剪裁得到电池空间
![abzOzV.jpg](https://s1.ax1x.com/2020/08/10/abzOzV.jpg)
####9.内扣顶部画更小圆，裁出走线空间
![aqSgw4.jpg](https://s1.ax1x.com/2020/08/10/aqSgw4.jpg)


[reference my solidworks "ball" data](https://drive.google.com/file/d/1HuA2a0yvhoo7RTu_quWc_0OLPFRDuZ8V/view?usp=sharing)